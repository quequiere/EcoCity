﻿using Eco.Gameplay.Players;
using Eco.Shared.Math;
using EcoCity.Data;
using EcoCity.Players;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcoCity.Cities
{

    public class City
    {
        [JsonProperty]
        public String id { get; private set; }

        [JsonProperty]
        public String name { get; private set; }

        [JsonProperty]
        public Dictionary<String,Resident> residents { get; private set; }

        [JsonConstructor]
        public City()
        {

        }

        public City(String name,User user)
        {
            this.id = EcoCity.data.cityData.getNewUUID();
            this.residents = new Dictionary<String, Resident>();
            this.name = name;

            residents.Add(user.SlgId, new Resident(user, PowerEnum.mayor));

            //Save included in this fuction, no need to save
            EcoCity.data.cityData.registerCity(this);
        }

   
        public void claim(Vector2i location)
        {
            EcoCity.data.claimData.registerClaim(this,location);
        }
 

        private void save()
        {
            EcoCity.data.cityData.save();
        }

        public byte getDistrictId()
        {
            return EcoCity.data.claimData.getDistrictByteId(this);
        }
    }
}
