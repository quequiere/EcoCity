﻿using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared.Math;
using Eco.Shared.Utils;
using EcoCity.Cities;
using EcoCity.Players;
using EcoColorLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EcoCity.Commands
{
    class SubAttribute : Attribute
    {
        public string description { get; private set; }
        public RightEnum rightNeeded { get; private set; }

        internal SubAttribute(String description, RightEnum rightNeeded)
        {
            this.description = description;
            this.rightNeeded = rightNeeded;
        }
    }

    public enum SubCommandsUser
    {
        [SubAttribute("Display all available commands", RightEnum.all)] help,
        [SubAttribute("[cityName] Create a new city, you will pay taxes if WorldLeader set them.", RightEnum.withoutCity)] createcity,
        [SubAttribute("[cityName] Display info about your city", RightEnum.all)] info,
        [SubAttribute("[amount] Deposite money in your city", RightEnum.resident)] deposite,
        [SubAttribute("[amount] Withdraw money from your city", RightEnum.assistant)] withdraw,
        [SubAttribute("[resident/assistant/mayor] [playerName] Change a resident's role", RightEnum.mayor)] changerole,
        [SubAttribute("Claim the chunk at your location", RightEnum.assistant)] claim,
    }


    public class UserCityCommand : IChatCommandHandler
    {

        [ChatCommand("city", "/city base commands for users", ChatAuthorizationLevel.User)]
        public static void cityCommandBase(User user, String argsString = "")
        {
            String[] args = argsString.Split(' ');

            if (args.Length <= 0 || args[0].Equals(""))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + "Please use /city " + ChatFormat.Red.Value + "help"));
            }
            else
            {
                if (!SubCommandsUser.TryParse(args[0], out SubCommandsUser sub))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"We can't find this sub command: {args[0]}"));
                }
                else
                {
                    switch(sub)
                    {
                        case SubCommandsUser.help:
                            displayHelp(user);
                            break;
                        case SubCommandsUser.createcity:
                            createCity(user, args);
                            break;
                        case SubCommandsUser.changerole:
                            changeRole(user, args);
                            break;
                        case SubCommandsUser.info:
                            displayCityInfo(user, args);
                            break;
                        case SubCommandsUser.claim:
                            claimLocation(user, args);
                            break;
                        default:
                            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"Sub command not code ! Please report to dev: {args[0]}"));
                            break;
                    }

                }
            }
        }






        //----------------------------------------------------------------------------------

        private static void claimLocation(User user, String[] args)
        {
            City c = EcoCity.data.cityData.getCityFromResidentId(user.SlgId);
            if(c==null)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Red.Value} You need to have a city to do that"));
                return;
            }

            Resident r = c.residents[user.SlgId];
            MemberInfo info = typeof(SubCommandsUser).GetField(Enum.GetName(typeof(SubCommandsUser), SubCommandsUser.claim));
            SubAttribute attloc = Attribute.GetCustomAttribute(info, typeof(SubAttribute)) as SubAttribute;

            if (!RightCheck.canPerform(user,c, attloc.rightNeeded))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Red.Value} You need more right to claim !"));
                return;
            }

            Vector3 loc = user.Position;
            WorldPosition2i chunkpos =  WorldPosition2i.FromWrapped(loc.XZi);

            if(!EcoCity.data.claimData.canClaimAtPosition(chunkpos))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Red.Value} You can't claim here, already claimed !"));
                return;
            }

            c.claim(loc.XZi);
            user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Green.Value} Claim added to the city !"));
            

        }

        private static void changeRole(User user, String[] args)
        {
            if (args.Length <= 2 || args[1].Equals(""))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"Please read /city help to use good arguments"));
                return;
            }

            City currentCity = EcoCity.data.cityData.getCityFromResidentId(user.SlgId);
            if (currentCity == null)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"You need to be part of a city to do that."));
                return;
            }

            if(!currentCity.residents[user.SlgId].residentPower.Equals(PowerEnum.mayor))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"You need to be mayor to do that"));
                return;
            }


            if(!PowerEnum.TryParse(args[1], out PowerEnum targetPower))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"This power doesn't exist !"));
                return;
            }

            User targetUser =  UserManager.FindUserByName(args[2]);
            if(targetUser == null)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"This user doesn't exist !"));
                return;
            }

            if(!currentCity.residents.ContainsKey(targetUser.SlgId))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"This user need to be in your city to change his power"));
                return;
            }

            // REMOVE FALSE, this is for debug
            if (!user.Equals(targetUser)&&false)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"You can't change your own role !"));
            }
            else
            {
                if (targetPower.Equals(PowerEnum.mayor))
                {
                    // to downgrade the mayor
                    currentCity.residents[user.SlgId].setPower(PowerEnum.assitant);
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"You are not the mayor anymore"));
                }

                currentCity.residents[targetUser.SlgId].setPower(targetPower);
            }



            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"Role has been updated !"));
            return;

        }

        private static void createCity(User user, String[] args)
        {

            if (args.Length <= 1 || args[1].Equals(""))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"Please add the city name to this command"));
                return;
            }

            String cityName = args[1];
            if (EcoCity.data.cityData.getCityByName(cityName)!=null)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"{cityName} city already exist !"));
                return;
            }

            City currentCity = EcoCity.data.cityData.getCityFromResidentId(user.SlgId);
            if (currentCity != null)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"You are already in {currentCity.name} city"));
                return;
            }


            City city = new City(cityName,user);

            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(EcoCity.prefixColored + $"City created with name: {cityName}"));
            ChatManager.ServerMessageToAllLoc(Text.Info(Text.Size(2f, $"{user.Name} created a new city named {cityName}")), false, Eco.Shared.Services.DefaultChatTags.General, Eco.Shared.Services.ChatCategory.Info);
        }

        private static void displayCityInfo(User user, String[] args)
        {
            City targetCity = null;
            if (args.Length <= 1 || args[1].Equals(""))
            {
                City ownCity= EcoCity.data.cityData.getCityFromResidentId(user.SlgId);
                if(ownCity==null)
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Red.Value} You haven't city, please add a city name to this command"));
                    return;
                }
                else
                {
                    targetCity = ownCity;
                }
            }
            else
            {
                City searchCity = EcoCity.data.cityData.getCityByName(args[1]);
                if (searchCity == null)
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Red.Value} Sorry we can't find city: {args[1]}"));
                    return;
                }
                else
                {
                    targetCity = searchCity;
                }
            }


            if(targetCity==null)
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create($"{EcoCity.prefixColored}{ChatFormat.Red.Value} Sorry this city can't be founded."));
                return;
            }

            StringBuilder title = new StringBuilder();
            StringBuilder text = new StringBuilder();
            title.Append($"{ChatFormat.Bold.Value}{ChatFormat.Green.Value}{targetCity.name} info {ChatFormat.Clear.Value}");

            text.AppendLine($"{ChatFormat.Red.Value}{targetCity.name} city:{ChatFormat.Clear.Value}");
            text.AppendLine($"");
            text.AppendLine($"{ChatFormat.Blue.Value} Residents: ({targetCity.residents.Count}){ChatFormat.Clear.Value}");
            foreach(Resident r in targetCity.residents.Values)
            {
                User t = UserManager.FindUserBySlgId(r.id);
                String name = r.id;
                if(t!=null)
                {
                    name = t.Name;
                }
                text.AppendLine($"{ChatFormat.Gray.Value}   -{name} {ChatFormat.Yellow.Value}{r.residentPower.ToString()}{ChatFormat.Clear.Value}");
            }

            user.Player.OpenInfoPanel(title.ToString(), text.ToString());
        }


        private static void displayHelp(User user)
        {
            StringBuilder title = new StringBuilder();
            StringBuilder text = new StringBuilder();
            title.Append("City help");

            City cityuser = EcoCity.data.cityData.getCityFromResidentId(user.SlgId);

            foreach (SubCommandsUser sub in Enum.GetValues(typeof(SubCommandsUser)))
            {
                MemberInfo info = typeof(SubCommandsUser).GetField(Enum.GetName(typeof(SubCommandsUser), sub));
                SubAttribute attloc = Attribute.GetCustomAttribute(info, typeof(SubAttribute)) as SubAttribute;
                if(RightCheck.canPerform(user, cityuser, attloc.rightNeeded))
                {
                    text.AppendLine("/city " + sub.ToString() + " : " + attloc.description);
                }
            }

            user.Player.OpenInfoPanel(title.ToString(), text.ToString());
        }
    }
}
