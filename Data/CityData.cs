﻿using EcoCity.Cities;
using EcoCity.Players;
using JsonConfigSaver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcoCity.Data
{
    public class CityData : JsonEcoConfig
    {
        [JsonProperty]
        private Dictionary<String, City> cityList = new Dictionary<string, City>();




        public CityData(string pluginName, string name) : base(pluginName, name)
        {

        }



        public void registerCity(City c)
        {
            this.cityList.Add(c.id, c);
            this.save();
            EcoCity.data.claimData.attributeNewIdCtyTemp(c);
        }




        public String getNewUUID()
        {
            Guid id = Guid.NewGuid();
            String ids = id.ToString();
            if(cityList.ContainsKey(ids))
            {
                ids = getNewUUID();
            }

            return ids;
        }

        public City getCityById(String id)
        {
            return cityList[id];
        }

        public List<City> getAllCity()
        {
            return cityList.Values.ToList();
        }

        public City getCityByName(String name)
        {
            City target = cityList.Values.Where(c => c.name.Equals(name)).FirstOrDefault();
            return target;
        }

        public City getCityFromResidentId(String slgId)
        {
            City city = cityList.Values.Where(c => c.residents.ContainsKey(slgId)).FirstOrDefault();
            return city;
        }


        public Resident getResidentFromResidentId(String slgId)
        {
            Resident r = cityList.Values.Where(c => c.residents.ContainsKey(slgId))
                .Select(c => c.residents[slgId])
                .FirstOrDefault();
            return r;
        }

        //public Resident getResidentFromResidentId(String slgId)
        //{
        //    City c = getCityFromResidentId(slgId);
        //    if (c == null)
        //        return null;

        //    Resident r = null;
        //    c.residents.TryGetValue(slgId,out r);
        //    return r;
        //}
    }
}
