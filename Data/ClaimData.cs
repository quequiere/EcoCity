﻿using Eco.Gameplay;
using Eco.Gameplay.LegislationSystem;
using Eco.Shared.Math;
using Eco.Shared.Utils;
using EcoCity.Cities;
using JsonConfigSaver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EcoCity.Data
{
    public class ClaimData : JsonEcoConfig
    {
        //city id VS chunk pos
        [JsonProperty]
        public Dictionary<Vector2i, String> claimed = new Dictionary<Vector2i, String>();


        private Dictionary<byte, String> cityCacheDistrictId = new Dictionary<byte, String>();
        private byte currentIdToAttribute;



        public ClaimData(string pluginName, string name) : base(pluginName, name)
        {

        }


        public override void postLoaded<ClaimData>(ClaimData data)
        {
            currentIdToAttribute = byte.MaxValue;
            foreach (City c in EcoCity.data.cityData.getAllCity())
            {
                attributeNewIdCtyTemp(c);
            }

            this.reloadDistricts();
        }

        public byte getDistrictByteId(City c)
        {
            byte id=cityCacheDistrictId.Keys.Where(b => cityCacheDistrictId[b].Equals(c.id)).FirstOrDefault();
            return id;
        }

        public void attributeNewIdCtyTemp(City c)
        {
            cityCacheDistrictId.Add(currentIdToAttribute, c.id);
            Console.WriteLine(EcoCity.prefix + currentIdToAttribute + " ==> byte attribute for data city " + c.name);
            currentIdToAttribute--;
        }

        public void registerClaim(City c, Vector2i v)
        {
            claimed.Add(v,c.id);
            this.reloadDistricts();
            this.save();
        }

        private District getDistrictAtPosition(WorldPosition2i pos)
        {
            Government g = Legislation.Government;
            Districts districts = g.Districts;
            District target = districts.GetDistrictAt(pos);
            return target;
        }

        public bool canClaimAtPosition(WorldPosition2i pos)
        {
            District d = getDistrictAtPosition(pos);
            if (d == null)
                return true;

            return false;
        }


        public City getCityAtPostion(WorldPosition2i pos)
        {
            District d = getDistrictAtPosition(pos);
            if (d == null)
                return null;

            if(cityCacheDistrictId.TryGetValue(d.ID,out string cityId))
            {
                return EcoCity.data.cityData.getCityById(cityId);
            }
            else
            {
                return null;
            }

        }


        private void reloadDiscritsIds()
        {
            Government g = Legislation.Government;
            Districts districts = g.Districts;
            FieldInfo fieldTarget = null;

            foreach (FieldInfo f in districts.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {

                if (f.Name.Contains("DistrictMetadata"))
                {
                    fieldTarget = f;
                    break;
                }
            }
            Dictionary<byte, District> metadata = fieldTarget.GetValue(districts) as Dictionary<byte, District>;

            foreach(City c in EcoCity.data.cityData.getAllCity())
            {
                metadata.Remove(c.getDistrictId());
                District d = new District(58, c.name, Color.Blue);
                metadata.Add(c.getDistrictId(), d);
            }

        }

        private void reloadDistricts()
        {
            this.reloadDiscritsIds();
        }
    }
}
