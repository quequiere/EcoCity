﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcoCity.Data
{
    public class GlobalData
    {
        public CityData cityData { get; private set; }
        public ClaimData claimData { get; private set; }


        public void registerData()
        {
            this.registerCityData();
            this.registerClaimData();
        }


        private void registerClaimData()
        {
            this.claimData = new ClaimData("EcoCity", "ClaimData");
            if (this.claimData.exist())
            {
                this.claimData = this.claimData.reload<ClaimData>();
            }
            else
            {
                this.claimData.save();
            }
        }

        private void registerCityData()
        {
            this.cityData = new CityData("EcoCity", "CityData");
            if (this.cityData.exist())
            {
                this.cityData = this.cityData.reload<CityData>();
            }
            else
            {
                this.cityData.save();
            }
        }

    }
}
