﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Eco.Gameplay.Players;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EcoCity.Players
{
    public class Resident
    {
        [JsonProperty]
        public String id { get; private set; }


        [JsonProperty]
        public PowerEnum residentPower;



        [JsonConstructor]
        public Resident()
        {
        }



        public Resident(User user, PowerEnum power)
        {
            this.id = user.SlgId;
            this.residentPower = power;
        }


        private void save()
        {
            EcoCity.data.cityData.save();
        }

        public void setPower(PowerEnum power)
        {
            this.residentPower = power;
            this.save();
        }

    }
}
