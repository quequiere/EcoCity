﻿using Eco.Gameplay.Players;
using EcoCity.Cities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcoCity.Players
{
    public enum RightEnum
    {
        administrator,
        all,
        mayor,
        assistant,
        resident,
        withoutCity
    }

    public class RightCheck
    {
        public static bool canPerform(User u,City c, RightEnum right)
        {
            if (right.Equals(RightEnum.all))
            {
                return true;
            }

            if (right.Equals(RightEnum.withoutCity))
            {
                if (EcoCity.data.cityData.getCityFromResidentId(u.SlgId) == null)
                {
                    return true;
                }
            }
            else if (c == null)
            {

            }
            else if (c.residents.ContainsKey(u.SlgId))
           {
                PowerEnum power = c.residents[u.SlgId].residentPower;

                if (right.Equals(RightEnum.resident))
                {
                    return true;
                }
                else if(right.Equals(RightEnum.assistant))
                {
                    if(power.Equals(PowerEnum.mayor) || power.Equals(PowerEnum.assitant))
                    {
                        return true;
                    }
                }
                else if (right.Equals(RightEnum.mayor))
                {
                    if (power.Equals(PowerEnum.mayor))
                    {
                        return true;
                    }
                }
            }
            else
            {
                // To check if player has perm on other city ==> usefull for server administrator
            }


            return false;
        }
    }
}
